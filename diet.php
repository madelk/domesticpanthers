<?php require 'header.php';?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="mt-5">Diet Information</h1>
            </div>
            <div class="col-lg-12">
<p>Panthers in the wild feed on small hoofed animals, such as deer, impala and gazelle. Some panthers also stalk and kill domestic animals, such as pigs and dogs, when human settlements are nearby. Panthers are strong swimmers and sometimes capture fish in the water. In captivity, they are generally fed cuts of raw meat, such as beef or pork.<br/>
<br/>
Cats are carnivores and the panther is at the top of the food chain in his natural environment. His dark, shiny coat makes it difficult for prey to spot him as this big cat hunts during the night. He's also a strong climber, so he has an added advantage in that he can climb into trees and ambush his prey from above. The black panther cat's diet depends on where he lives, but medium to large herbivores, such as antelope, deer, warthogs and wild boar, are a mainstay of his diet. He's an opportunistic diner, however, and if his normal prey is scarce, the black panther cat will make do with what he finds, eating rabbits, birds, fish and reptiles -- or even insects.
</p>
</div>
            </div>
        </div>
    </div>
<?php require 'footer.php';?>