<?php require 'header.php';?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="mt-5">Behavior</h1>
            </div>
            <p>Black panthers have unique behaviors or habits which are different from those of other big cats or pet cats.  As members of the big cat family, black panthers can roar while other small cats, such as house cats, bobcats, lynx, and cougars, purr but cannot roar.</p><p>
Black panthers are elusive animals.  People rarely see them in the wild, even though their existence has been confirmed across vast areas of South and Southeast Asia, Central and South America and some parts of Africa.  Because the black panther is so stealthy, it has been called the ghost of the forest.  The black panther is also solitary.  Other than a female and her cubs, or mating pairs in the breeding season, these animals seldom stay together. Each of them lives and hunts by itself in an area known as the home range.  Black panthers communicate with one another with signs and vocalizations used mostly for maintaining their home ranges as well as for signaling mating partners.</p><p>
Adult black panthers are more temperamental than their normal-colored kin.  This is because they tend to be more inbred than their fair-colored counterparts.  Black panthers are less fertile, too.</p><p>
Like many of the smaller cats and unlike most of the other big cats, black panthers are the strongest climbers of the cat family.  A black panther develops tree-climbing skills at a very early age to avoid attack from deadly enemies such as lions and hyenas.  It also uses this unique skill to pull its kills or carcasses up to a tree.</p>
        </div>
    </div>
<?php require 'footer.php';?>