<?php require 'header.php';?>
<div class="container">
    <div class="row">
        <img src="images/eyes.jpg" class="img-fluid"/>
        <div class="col-lg-12 text-center">
            <h1 class="mt-5">Facts</h1>
        </div>
        <ol>
            <li>
                Panther is known as black jaguar in Latin America, as black leopard in Asia and Africa, and as black cougar in North America.</li>
            <li>
                Adult animal has 7-8 feet in length and weighs between 100-250 pounds.</li>
            <li>
                They are carnivores (meat-eaters). They hunt and eat everything from birds and reptiles to large mammals.</li>
            <li>
                Panther can produce loud roar.</li>
            <li>
                Panthers are solitary animals. They will meet only during mating season.</li>
            <li>
                After three months of pregnancy, female will give birth to 2-4 babies. She takes care of them by herself.</li>
            <li>
                Panthers learn to climb the tree early in their life. They are strongest tree climbers in the cat world.</li>
            <li>
                Eyes of the newly born cub are closed and covered with light fur.</li>
            <li>
                At age of 2-3 months, cubs learn how to hunt from their mother.</li>
            <li>
                Just nine months after birth, young panthers can catch medium-sized prey. They learn quickly.</li>
            <li>
                Black panthers have large and strong paws and sharp claws that are used for hunting.</li>
            <li>
                Panther can survive in areas populated with humans better than other cats.</li>
            <li>
                Panther can leap up to 20 feet.</li>
            <li>
                They have excellent eyesight and sense of hearing.</li>
            <li>
                In the wild, panthers can live up to 12 years and in captivity up to 20.</li>
        </ol>
    </div>
    <div class="col-lg-12 text-center">
        <h1 class="mt-5">Behavior</h1>
    </div>
    <p>Black panthers have unique behaviors or habits which are different from those of other big cats or pet cats. As members of the big cat family, black panthers can roar while other small cats, such as house cats, bobcats, lynx, and cougars, purr but
        cannot roar.</p>
    <p>
        Black panthers are elusive animals. People rarely see them in the wild, even though their existence has been confirmed across vast areas of South and Southeast Asia, Central and South America and some parts of Africa. Because the black panther is so stealthy,
        it has been called the ghost of the forest. The black panther is also solitary. Other than a female and her cubs, or mating pairs in the breeding season, these animals seldom stay together. Each of them lives and hunts by itself in an area known
        as the home range. Black panthers communicate with one another with signs and vocalizations used mostly for maintaining their home ranges as well as for signaling mating partners.</p>
    <p>
        Adult black panthers are more temperamental than their normal-colored kin. This is because they tend to be more inbred than their fair-colored counterparts. Black panthers are less fertile, too.</p>
    <p>
        Like many of the smaller cats and unlike most of the other big cats, black panthers are the strongest climbers of the cat family. A black panther develops tree-climbing skills at a very early age to avoid attack from deadly enemies such as lions and hyenas.
        It also uses this unique skill to pull its kills or carcasses up to a tree.</p>

        <img src="images/mating.jpg" class="img-fluid"/>
    <div class="col-lg-12 text-center">
        <h1 class="mt-5">Diet Information</h1>
    </div>
    <div class="col-lg-12">
        <p>Panthers in the wild feed on small hoofed animals, such as deer, impala and gazelle. Some panthers also stalk and kill domestic animals, such as pigs and dogs, when human settlements are nearby. Panthers are strong swimmers and sometimes capture
            fish in the water. In captivity, they are generally fed cuts of raw meat, such as beef or pork.<br/>
            <br/> Cats are carnivores and the panther is at the top of the food chain in his natural environment. His dark, shiny coat makes it difficult for prey to spot him as this big cat hunts during the night. He's also a strong climber, so he has
            an added advantage in that he can climb into trees and ambush his prey from above. The black panther cat's diet depends on where he lives, but medium to large herbivores, such as antelope, deer, warthogs and wild boar, are a mainstay of his
            diet. He's an opportunistic diner, however, and if his normal prey is scarce, the black panther cat will make do with what he finds, eating rabbits, birds, fish and reptiles -- or even insects.
        </p>
    </div>
    <div class="col-lg-12 text-center">
        <h1 class="mt-5">Mating and Reproduction Information</h1>
    </div>
    <p>Gestational period is 90 to 105 days.</p>
    <p>
        A black panther cub is usually born in the same litter along with other light-colored leopard cubs. Typically, a leopard litter consists of two to three cubs but, sometimes, up to six. Black panther cubs are born with their eyes closed and are covered
        with faintly spotted smoky gray fur. They weigh about 450 to 1000 g (16 to 35 oz). The mother leopard has to stay at the den all the time during the first few days after the birth to rest and nurse the newborn baby panthers. Meanwhile cubs spend
        most of their time sleeping and nursing on their mother’s rich milk. About ten days after their birth, the cubs open their glazed eyes and get their first glimpse of the world. While the cubs still lack of mobility, the mother leopard have to
        leave her cubs unattended as she travels far from the birth den to find food for her family. This is the period when the defenseless cubs are most vulnerable to predators; therefore, the choice of a site for a birth den is so crucial to the safety
        of baby black panther cubs. By the age of two, panthers are fully independent.</p>
        
        <img src="images/habitat.jpg" class="img-fluid"/>
    <div class="col-lg-12 text-center">
        <h1 class="mt-5">Natural Habitat</h1>
    </div>
    <p></p> An animal's habitat is the place where its basic needs for food, water, shelter, and reproduction are met. Black panthers are adapted to living in a wide variety of habitats within their range. The black panthers habitats include the rainforest,
    marshland, woodlands, swamps, savannahs, and even mountains and deserts.</p>
    <p>
        One of the reasons that black panthers are able to live in such variety of habitats is that they can eat many types of animals. Their food includes various species of mammals, reptiles, and birds, all of which live in different habitats. They are also
        able to live in human-populated areas more effectively than any other big cats if they have to.</p>
    <div class="col-lg-12 text-center">
        <h1 class="mt-5">Animal Communication</h1>
    </div>
    <p>It’s true that animals always try communicating with humans but we are often not listening. This journey of a panther who was abused in the past is so inspiring and touching at the same time. The soul energy exists in all humans and animals, and when
        aligned on the same level is where the magic happens. This animal whisperer proves to us that she can talk to animals and understand their needs and wants, including finding out about their past very accurately. It’s completely mind blowing that
        what the panther told the animal whisperer has been confirmed as true by everyone who knows about the panther’s past. If this incredible story touched your soul, please do share this post to inspire everyone around you.</p>

    <div class="col-lg-12 text-center">
        <h1 class="mt-5">Anatomy and Biology</h1>
    </div>
    <p>The Panther tends to be dark brown to black in colour and is otherwise identical to the feline species to which it belongs. The only real exception to this is the Florida Panther found in the south east region of the USA, that is believed to be a
        subspecies of Cougar and is quite rarely dark brown in colour and tends to have more of a speckled appearance. Unlike Leopards and Jaguars, the Panther has no spots on its long body or tail, but instead has a shiny coat of dark fur. Panthers have
        small heads with strong jaws and emerald green eyes, and tend to have hind legs that are both larger and slightly longer than those at the front. Being a member of the Big Cat family, the Panther is not only one of the largest felines in the world
        but it is also able to roar which is something that felines outside of this group are not able to do.</p>
</div>
</div>
<?php require 'footer.php';?>
