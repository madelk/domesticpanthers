<?php require 'header.php';?>
    <div class="container" style="margin-top: -70px;">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="mt-5">Domestic Panthers</h1>
                <h2>Presented by the Scrum Zoo</h2>
                <p class="lead" style="font-size:1.1em;"><p><i style="font-size:1.1em;">Friendly yet Frightening</i></p>

<p>Everything you need to know to safely domesticate your own panther.</p>
<p>First, to domesticate a panther, you need to know the facts about panthers.  See our pages about the diet, habits, reproduction, and more.</p>
<p>A domesticated panther is a badge of honor and makes for great party conversation.</p>
                <ul class="list-unstyled">
                    <li>Black domestic Panthers are the melanistic color variant of any panther species</li>
                    <li>Black panthers in Asia and Africa are leopards</li>
                    <li>In America - Jaguars</li>
                </ul>
                <img src="/images/HomePage.png" class="img-fluid" />
            </div>
            
        </div>
    </div>
<?php require 'footer.php';?>