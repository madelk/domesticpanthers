<?php 
$directoryURI = $_SERVER['REQUEST_URI'];
$path = parse_url($directoryURI, PHP_URL_PATH);
$components = explode('/', $path);
$first_part = $components[1];
?>
<nav class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse" id="navigation">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="container">
        <a class="navbar-brand" href="/">Domestic Panther</a>
        <div class="collapse navbar-collapse" id="navbarExample">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?php if ($first_part=="") {echo "active"; } ?>"> 
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item <?php if ($first_part=="images.php") {echo "active"; } ?>">
                    <a class="nav-link" href="/images.php">Images</a>
                </li>
                <!--<li class="nav-item <?php if ($first_part=="diet.php") {echo "active"; } ?>">-->
                <!--    <a class="nav-link" href="/diet.php">Diet</a>-->
                <!--</li>-->
                <!--<li class="nav-item <?php if ($first_part=="behavior.php") {echo "active"; } ?>">-->
                <!--    <a class="nav-link" href="/behavior.php">Behavior</a>-->
                <!--</li>-->
                <!--<li class="nav-item <?php if ($first_part=="mating.php") {echo "active"; } ?>">-->
                <!--    <a class="nav-link" href="/mating.php">Mating and Reproduction</a>-->
                <!--</li>-->
                <!--<li class="nav-item <?php if ($first_part=="naturalhabitat.php") {echo "active"; } ?>">-->
                <!--    <a class="nav-link" href="/naturalhabitat.php">Natural Habitat</a>-->
                <!--</li>-->
                <li class="nav-item <?php if ($first_part=="facts.php") {echo "active"; } ?>">
                    <a class="nav-link" href="/facts.php">Facts</a>
                </li>
                <!--<li class="nav-item <?php if ($first_part=="communication.php") {echo "active"; } ?>">-->
                <!--    <a class="nav-link" href="/communication.php">Communication</a>-->
                <!--</li>-->
                <!--<li class="nav-item <?php if ($first_part=="anatomy.php") {echo "active"; } ?>">-->
                <!--    <a class="nav-link" href="/anatomy.php">Anatomy and Biology</a>-->
                <!--</li>-->
            </ul>
        </div>
    </div>
</nav>