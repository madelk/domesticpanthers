<?php require 'header.php';?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="mt-5">Anatomy and Biology</h1>
            </div>
            <p>The Panther tends to be dark brown to black in colour and is otherwise identical to the feline species to which it belongs. The only real exception to this is the Florida Panther found in the south east region of the USA, that is believed to be a subspecies of Cougar and is quite rarely dark brown in colour and tends to have more of a speckled appearance. Unlike Leopards and Jaguars, the Panther has no spots on its long body or tail, but instead has a shiny coat of dark fur. Panthers have small heads with strong jaws and emerald green eyes, and tend to have hind legs that are both larger and slightly longer than those at the front. Being a member of the Big Cat family, the Panther is not only one of the largest felines in the world but it is also able to roar which is something that felines outside of this group are not able to do.</p>
        </div>
    </div>
<?php require 'footer.php';?>