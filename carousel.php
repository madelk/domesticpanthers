<div id="carouselExampleIndicators" class="carousel slide center-text text-center" style="margin-left:auto; margin-right:auto;" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <a href="images/1.jpg" target="_blank">
        <img class="d-block img-fluid" src="images/1.jpg" alt="domesticpanther">
        <div class="carousel-caption d-none d-md-block">
        <h3>A Black Panther</h3>
      </div>
      </a>
    </div>
    <div class="carousel-item">
      <a href="images/2.jpg" target="_blank">
      <img class="d-block img-fluid" src="images/2.jpg" alt="domesticpanther">
        <div class="carousel-caption d-none d-md-block">
        <h3>A Black Panther</h3>
      </div>
      </a>
    </div>
    <div class="carousel-item">
      <a href="images/3.jpg" target="_blank">
      <img class="d-block img-fluid" src="images/3.jpg" alt="domesticpanther">
        <div class="carousel-caption d-none d-md-block">
        <h3>A Black Panther</h3>
      </div>
      </a>
    </div>
    <div class="carousel-item">
      <a href="images/4.jpg" target="_blank">
      <img class="d-block img-fluid" src="images/4.jpg" alt="domesticpanther">
        <div class="carousel-caption d-none d-md-block">
        <h3>A Black Panther</h3>
      </div>
      </a>
    </div>
    <div class="carousel-item">
      <a href="images/5.jpg" target="_blank">
      <img class="d-block img-fluid" src="images/5.jpg" alt="domesticpanther">
        <div class="carousel-caption d-none d-md-block">
        <h3>A Black Panther</h3>
      </div>
      </a>
    </div>
    <!--<div class="carousel-item">-->
    <!--  <img class="d-block img-fluid" src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Chicago">-->
    <!--  <div class="carousel-caption d-none d-md-block">-->
    <!--    <h3>Chicago</h3>-->
    <!--    <p>More Info</p>-->
    <!--  </div>-->
    <!--</div>-->
    <!--<div class="carousel-item">-->
    <!--  <img class="d-block img-fluid" src="https://www.w3schools.com/bootstrap/ny.jpg" alt="New York">-->
    <!--  <div class="carousel-caption d-none d-md-block">-->
    <!--    <h3>New York</h3>-->
    <!--    <p>More Info</p>-->
    <!--  </div>-->
    <!--</div>-->
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>