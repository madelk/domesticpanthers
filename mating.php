<?php require 'header.php';?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="mt-5">Mating and Reproduction Information</h1>
            </div>
            <p>Gestational period is 90 to 105 days.</p><p>
A black panther cub is usually born in the same litter along with other light-colored leopard cubs.  Typically, a leopard litter consists of two to three cubs but, sometimes, up to six.  Black panther cubs are born with their eyes closed and are covered with faintly spotted smoky gray fur.  They weigh about 450 to 1000 g (16 to 35 oz).
The mother leopard has to stay at the den all the time during the first few days after the birth to rest and nurse the newborn baby panthers.  Meanwhile cubs spend most of their time sleeping and nursing on their mother’s rich milk.  About ten days after their birth, the cubs open their glazed eyes and get their first glimpse of the world.  While the cubs still lack of mobility, the mother leopard have to leave her cubs unattended as she travels far from the birth den to find food for her family.  This is the period when the defenseless cubs are most vulnerable to predators; therefore, the choice of a site for a birth den is so crucial to the safety of baby black panther cubs.
By the age of two, panthers are fully independent.</p>
        </div>
    </div>
<?php require 'footer.php';?>