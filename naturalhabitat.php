<?php require 'header.php';?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="mt-5">Natural Habitat</h1>
            </div>
          <p></p>  An animal's habitat is the place where its basic needs for food, water, shelter, and reproduction are met.  Black panthers are adapted to living in a wide variety of habitats within their range.  The black panthers habitats include the rainforest, marshland, woodlands, swamps, savannahs, and even mountains and deserts.</p>
<p>
One of the reasons that black panthers are able to live in such variety of habitats is that they can eat many types of animals.  Their food includes various species of mammals, reptiles, and birds, all of which live in different habitats.  They are also able to live in human-populated areas more effectively than any other big cats if they have to.</p>
</div>
    </div>
<?php require 'footer.php';?>